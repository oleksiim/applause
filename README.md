# ApplauseApp

by Oleksii Melnykov
https://www.linkedin.com/in/oleksii-melnykov-82948999/

email: oleksii.melnykov@gmail.com


Here is my implementation of an Applause App task. 
According to the task I've created a simple app for representation of Tester Experience.

![Alt text](./src/assets/screenshot.png?raw=true "Optional Title") 

To be able to run the app follow next steps:
### Step 1
Clone this project `git clone https://oleksiim@bitbucket.org/oleksiim/applause.git`

### Step 2
After cloning is done navigate into the project folder and run command:
`npm i`

### Step 3
Once npm downloaded all the dependencies run `ng serve` for starting a dev server. 

### Step 4
Navigate to `http://localhost:4200/`. 



####External dependencies were used:
* Bootstrap 4 (for styling)
* ag-grid-angular (ready to use tables)
* ngx-papaparse (for parsing .csv files)

