import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { AgGridModule } from 'ag-grid-angular';
import { CountryFilterComponent } from './country-filter/country-filter.component';
import { DeviceFilterComponent } from './device-filter/device-filter.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [HomeComponent, CountryFilterComponent, DeviceFilterComponent],
  imports: [
    CommonModule,
    AgGridModule,
    FormsModule
  ]
})
export class HomeModule { }
