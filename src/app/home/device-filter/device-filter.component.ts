import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Device } from '../../services/data/devices.service';

@Component({
  selector: 'app-device-filter',
  templateUrl: './device-filter.component.html',
  styleUrls: ['./device-filter.component.scss']
})
export class DeviceFilterComponent implements OnInit {

  private allDevices = [];
  checkboxes = [];

  @Input()
  set devices(val: Array<Device>) {
    this.allDevices = val;
    this.checkboxes = val.map(device => {
      return {'device': device, 'checked': true};
    });
  }

  @Output() changed: EventEmitter<Array<Device>> = new EventEmitter<Array<Device>>();
  @Input() value: Array<Device> = [];

  ngOnInit() {
  }

  toggleDevice(device: Device, input: HTMLInputElement) {
    if (this.value.some(el => el.id === device.id)) {
      this.value = this.value.filter(_device => _device !== device);
    } else {
      this.value.push(device);
    }
    this.checkboxes.filter(checkbox => checkbox.device.id === device.id)[0].checked = input.checked;
    this.changed.emit(this.value);
  }

  selectAll() {
    if (this.isAllChecked()) {
      this.value = [];
      this.checkboxes.forEach(_ => _.checked = false);
    } else {
      this.value = this.allDevices;
      this.checkboxes.forEach(_ => _.checked = true);
    }
    this.changed.emit(this.value);
  }

  isAllChecked() {
    return this.checkboxes.every(_ => _.checked);
  }

}
