import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-country-filter',
  templateUrl: './country-filter.component.html',
  styleUrls: ['./country-filter.component.scss']
})
export class CountryFilterComponent implements OnInit {

  private allCountries = [];
  checkboxes = [];

  @Input()
  set countries(val: Array<string>) {
    this.allCountries = val;
    this.checkboxes = val.map(country => {
      return {'country': country, 'checked': true};
    });
  }

  @Output() changed: EventEmitter<Array<string>> = new EventEmitter<Array<string>>();
  @Input() value: Array<string> = [];


  ngOnInit() {
  }

  toggleCountry(country: string, input: HTMLInputElement) {
    if (this.value.indexOf(country) === -1) {
      this.value.push(country);
    } else {
      this.value = this.value.filter(_country => _country !== country);
    }
    this.checkboxes.filter(checkbox => checkbox.country === country)[0].checked = input.checked;
    this.changed.emit(this.value);
  }

  selectAll() {
    if (this.isAllChecked()) {
      this.value = [];
      this.checkboxes.forEach(_ => _.checked = false);
    } else {
      this.value = this.allCountries;
      this.checkboxes.forEach(_ => _.checked = true);
    }
    this.changed.emit(this.value);
  }

  isAllChecked() {
    return this.checkboxes.every(_ => _.checked);
  }
}
