import { Component, OnInit } from '@angular/core';
import { Bug, BugsService } from '../services/data/bugs.service';
import { Device, DevicesService } from '../services/data/devices.service';
import { Tester, TesterDevice, TesterService } from '../services/data/tester.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  // columns and rows of the first table
  columnDefs = [
    {headerName: 'Tester', field: 'tester', filter: true, lockPosition: true},
    {headerName: 'Experience', field: 'bugsCount', lockPosition: true},
  ];
  rowData = [];

  // columns and rows of the second table
  columnDefs2 = [
    {headerName: 'Tester', field: 'tester', filter: true, lockPosition: true},
    {headerName: 'Device', field: 'device', filter: true, lockPosition: true},
    {headerName: 'Experience per device', field: 'bugsPerDevice', lockPosition: true},
  ];
  rowData2 = [];

  bugsList: Array<Bug> = [];
  devicesList: Array<Device> = [];
  testersList: Array<Tester> = [];
  testerDeviceList: Array<TesterDevice> = [];
  selectedDevices = [];
  countries = [];
  selectedCountries = [];

  constructor(private bugService: BugsService,
              private devicesService: DevicesService,
              private testerService: TesterService) {
  }

  ngOnInit() {
    const bugPromise = this.bugService.getBugs().then(data => {
      this.bugsList = data;
    });

    const devicePromise = this.devicesService.getDevices().then(data => {
      this.devicesList = data;
      this.selectedDevices = this.devicesList;
    });

    const testerPromise = this.testerService.getTesters().then(data => {
      this.testersList = data;
      this.countries = [...new Set(data.map(obj => obj.country))];
      this.selectedCountries = this.countries;
    });

    const testerDevicePromise = this.testerService.getTesterDevice().then(data => {
      this.testerDeviceList = data;
    });

    Promise.all([bugPromise, devicePromise, testerPromise, testerDevicePromise])
    .then(values => {
      this.applyFilters();
    });

  }

  setCountryFilter(countries: Array<string>) {
    this.selectedCountries = countries;
    this.applyFilters();
  }

  setDeviceFilter(devices: Array<Device>) {
    this.selectedDevices = devices;
    this.applyFilters();
  }

  applyFilters() {
    // filter testers by selected country
    const filteredTestersList = this.testersList.filter(tester => this.selectedCountries.indexOf(tester.country) !== -1);
    // filter bug list by testers from selected country and by selected devices
    const filteredBugsList = this.bugsList.filter(
      bug => filteredTestersList.some(tester => tester.id === bug.testerId)
        && this.selectedDevices.some(device => device.id === bug.deviceId)
    );
    this.fillTableRows(filteredTestersList, filteredBugsList);
    this.fillSecondTableRows(filteredTestersList, filteredBugsList);
  }

  fillTableRows(testersList: Array<Tester>, bugsList: Array<Bug>) {
    // Data for first Table with testers overall experience
    this.rowData = testersList.map(tester => {
      return {
        'tester': `${tester.firstName}  ${tester.lastName}  (${tester.country})`,
        'bugsCount': bugsList.filter(bug => bug.testerId === tester.id).length
      };
    }).sort((a, b) => {
      return b.bugsCount - a.bugsCount;
    });
  }

  fillSecondTableRows(testersList: Array<Tester>, bugsList: Array<Bug>) {
    // Data for second table with testers experience per device
    const arrOfData = testersList.map(tester => {
      const testerDeviceList = this.testerDeviceList.filter(el => el.testerId === tester.id);
      return testerDeviceList.map(device => {
        return {
          'tester': `${tester.firstName}  ${tester.lastName} (${tester.country})`,
          'device': this.devicesList.filter((d) => d.id === device.deviceId)[0].description,
          'bugsPerDevice': bugsList.filter(bug => bug.testerId === tester.id && bug.deviceId === device.deviceId).length,
          'bugsTotal': bugsList.filter(bug => bug.testerId === tester.id).length
        };
      });
    });
    // merging array of arrays
    this.rowData2 = [].concat.apply([], arrOfData).sort((a, b) => {
      return b.bugsTotal - a.bugsTotal;
    });
  }
}
