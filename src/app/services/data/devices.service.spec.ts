import { TestBed } from '@angular/core/testing';

import { DevicesService } from './devices.service';
import { HttpPromiseService } from '../http/http-promise.service';
import { Papa } from 'ngx-papaparse';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('DevicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ],
    providers: [HttpPromiseService, Papa]
  }));

  it('should be created', () => {
    const service: DevicesService = TestBed.get(DevicesService);
    expect(service).toBeTruthy();
  });
});
