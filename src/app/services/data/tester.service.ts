import { Injectable } from '@angular/core';
import { HttpPromiseResponse, HttpPromiseService } from '../http/http-promise.service';
import { API } from '../../app.constant';
import { Papa } from 'ngx-papaparse';

@Injectable({
  providedIn: 'root',
})
export class TesterService {

  constructor(private http: HttpPromiseService, private papa: Papa) {
  }

  getTesters(): Promise<Tester | any> {
    return new Promise((resolve, reject) => {
      this.http.get(API.TESTERS_URL).then((response: HttpPromiseResponse) => {

        let testersList = [];
        this.papa.parse(response.payload, {
          complete: (result) => {
            // Step 1: Filter out empty rows
            testersList = result.data.filter(arr => {
              return arr.length > 1;
            });
            // Step 2: Remove first row, because this row contains column names
            testersList.shift();
            testersList = testersList.map(row => Tester.fromDto(row));
          }
        });

        resolve(testersList);
      }).catch((response: HttpPromiseResponse) => {
        reject(response);
      });
    });
  }

  getTesterDevice(): Promise<TesterDevice | any> {
    return new Promise((resolve, reject) => {
      this.http.get(API.TESTER_DEVICES_URL).then((response: HttpPromiseResponse) => {

        let testerDeviceList = [];
        this.papa.parse(response.payload, {
          complete: (result) => {
            // Step 1: Filter out empty rows
            testerDeviceList = result.data.filter(arr => {
              return arr.length > 1;
            });
            // Step 2: Remove first row, because this row contains column names
            testerDeviceList.shift();
            testerDeviceList = testerDeviceList.map(row => TesterDevice.fromDto(row));
          }
        });

        resolve(testerDeviceList);
      }).catch((response: HttpPromiseResponse) => {
        reject(response);
      });
    });
  }
}


export class Tester {

  constructor(id: number,
              firstName: string,
              lastName: string,
              country: string,
              lastLogin: string) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.country = country;
    this.lastLogin = lastLogin;
  }

  id: number;
  firstName: string;
  lastName: string;
  country: string;
  lastLogin: string;

  static fromDto(dto: Object): Tester {
    return new Tester(+dto[0].replace(/\"/g, ' '), dto[1], dto[2], dto[3], dto[4]);
  }
}

export class TesterDevice {

  constructor(testerId: number, deviceId: number) {
    this.testerId = testerId;
    this.deviceId = deviceId;
  }

  testerId: number;
  deviceId: number;

  static fromDto(dto: Object): TesterDevice {
    return new TesterDevice(+dto[0].replace(/\"/g, ' '), +dto[1]);
  }
}
