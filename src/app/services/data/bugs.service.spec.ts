import { TestBed } from '@angular/core/testing';

import { BugsService } from './bugs.service';
import { HttpPromiseService } from '../http/http-promise.service';
import { Papa } from 'ngx-papaparse';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('BugsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ],
    providers: [HttpPromiseService, Papa]
  }));

  it('should be created', () => {
    const service: BugsService = TestBed.get(BugsService);
    expect(service).toBeTruthy();
  });
});
