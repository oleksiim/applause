import { Injectable } from '@angular/core';
import { HttpPromiseResponse, HttpPromiseService } from '../http/http-promise.service';
import { API } from '../../app.constant';
import { Papa } from 'ngx-papaparse';

@Injectable({
  providedIn: 'root',
})
export class DevicesService {

  constructor(private http: HttpPromiseService, private papa: Papa) {
  }

  getDevices(): Promise<Device | any> {
    return new Promise((resolve, reject) => {
      this.http.get(API.DEVICES_URL).then((response: HttpPromiseResponse) => {
        let devicesList = [];
        this.papa.parse(response.payload, {
          complete: (result) => {
            // Step 1: Filter out empty rows
            devicesList = result.data.filter(arr => {
              return arr.length > 1;
            });
            // Step 2: Remove first row, because this row contains column names
            devicesList.shift();
            devicesList = devicesList.map(row => Device.fromDto(row));
          }
        });
        resolve(devicesList);
      }).catch((response: HttpPromiseResponse) => {
        reject(response);
      });
    });
  }
}


export class Device {

  constructor(id: number, description: string) {
    this.id = id;
    this.description = description;
  }

  id: number;
  description: string;

  static fromDto(dto: Object): Device {
    return new Device(+dto[0].replace(/\"/g, ' '), dto[1]);
  }
}
