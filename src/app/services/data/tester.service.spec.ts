import { TestBed } from '@angular/core/testing';

import { TesterService } from './tester.service';
import { HttpPromiseService } from '../http/http-promise.service';
import { Papa } from 'ngx-papaparse';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('TesterService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ],
    providers: [HttpPromiseService, Papa]
  }));

  it('should be created', () => {
    const service: TesterService = TestBed.get(TesterService);
    expect(service).toBeTruthy();
  });
});
