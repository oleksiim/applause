import { Injectable } from '@angular/core';
import { HttpPromiseResponse, HttpPromiseService } from '../http/http-promise.service';
import { API } from '../../app.constant';
import { Papa } from 'ngx-papaparse';

@Injectable({
  providedIn: 'root',
})
export class BugsService {

  constructor(private http: HttpPromiseService, private papa: Papa) {
  }

  getBugs(): Promise<Bug | any> {
    return new Promise((resolve, reject) => {
      this.http.get(API.BUGS_URL).then((response: HttpPromiseResponse) => {
        let bugsList = [];
        this.papa.parse(response.payload, {
          complete: (result) => {
            // Step 1: Filter out empty rows
            bugsList = result.data.filter(arr => {
              return arr.length > 1;
            });
            // Step 2: Remove first row, because this row contains column names
            bugsList.shift();
            bugsList = bugsList.map(row => Bug.fromDto(row));
          }
        });
        resolve(bugsList);
      }).catch((response: HttpPromiseResponse) => {
        reject(response);
      });
    });
  }
}

export class Bug {

  constructor(id: number, deviceId: number, testerId: number) {
    this.id = id;
    this.deviceId = deviceId;
    this.testerId = testerId;
  }

  id: number;
  deviceId: number;
  testerId: number;

  static fromDto(dto: Object): Bug {
    return new Bug(+dto[0].replace(/\"/g, ' '), +dto[1], +dto[2]);
  }
}
