import { TestBed } from '@angular/core/testing';

import { HttpPromiseService } from './http-promise.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('HttpPromiseService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ],
    providers: [HttpPromiseService]
  }));

  it('should be created', () => {
    const service: HttpPromiseService = TestBed.get(HttpPromiseService);
    expect(service).toBeTruthy();
  });
});
