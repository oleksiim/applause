import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';

@Injectable()
export class HttpPromiseService {

  constructor(private http: HttpClient) {
  }

  get(url: string, params?: any): Promise<HttpPromiseResponse> {
    return this.http.get(url, {observe: 'response', responseType: 'text', params: params ? params : {}})
    .toPromise()
    .then(this.extractData)
    .catch(this.handleErrorPromise);
  }

  private extractData(res: HttpResponse<Object>): HttpPromiseResponse {
    return {
      payload: res.body || res || {},
      status: res.status,
      statusText: res.statusText,
      ok: res.ok || false,
      headers: res.headers
    };
  }

  private handleErrorPromise(error: HttpErrorResponse | any) {
    return Promise.reject(error);
  }
}

export interface HttpPromiseResponse {
  payload: any;
  status: number;
  statusText: string;
  ok: boolean;
  headers: HttpHeaders;
}
