import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PapaParseModule } from 'ngx-papaparse';
import { HomeModule } from './home/home.module';
import { HttpClientModule } from '@angular/common/http';
import { BugsService } from './services/data/bugs.service';
import { HttpPromiseService } from './services/http/http-promise.service';
import { AgGridModule } from 'ag-grid-angular';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { DevicesService } from './services/data/devices.service';
import { TesterService } from './services/data/tester.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PapaParseModule,
    HomeModule,
    HttpClientModule,
    AgGridModule.withComponents([])
  ],
  providers: [
    HttpPromiseService,
    BugsService,
    DevicesService,
    TesterService
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
