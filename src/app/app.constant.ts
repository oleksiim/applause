export class API {
  static BUGS_URL = 'assets/bugs.csv';
  static DEVICES_URL = 'assets/devices.csv';
  static TESTER_DEVICES_URL = 'assets/tester_device.csv';
  static TESTERS_URL = 'assets/testers.csv';
}
